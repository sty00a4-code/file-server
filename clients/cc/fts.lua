---splits the string by the given seperators in `...`
---@param s string
---@param ... string
---@return table<integer, string>
string.split = function (s, ...)
    local seps = {...}
    local t = {}
    local temp = ""
    local idx = 1
    while idx <= #s do
        local c = s:sub(idx, idx)
		local found = false
        for _, sep in ipairs(seps) do
            if c == sep then
                if #temp > 0 then
                    table.insert(t, temp)
                    temp = ""
                end
				found = true
				break
            end
        end
		if not found then temp = temp .. c end
		idx = idx + 1
    end
    if #temp > 0 then
        table.insert(t, temp)
    end
    return t
end
local FilePath = {
    mt = {
        __name = "file-path"
    }
}
http.create = function (url, headers, binary)
    return http.request {
        url = url,
        method = "CREATE",
        headers = headers,
        binary = binary,
    }
end
---@param path string
---@return FilePath
function FilePath.from(path)
    local path = path:split("/", "\\")
    local head = {}
    for i = 1, #path - 1 do
        head[i] = table.remove(path, 1)
    end
    local file = table.remove(path, 1)
    ---@class FilePath
    return setmetatable({
        head = table.concat(head, "/"),
        file = file,

        full = FilePath.full,
        removeRoot = FilePath.removeRoot
    }, FilePath.mt)
end
---@param self FilePath
function FilePath:full()
    return self.head.."/"..self.file
end
---@param self FilePath
function FilePath:removeRoot()
    local start = 1
    while start <= #self.head do
        if self.head:sub(start, start) == "/" then
            start = start + 1
            break
        end
        start = start + 1
    end
    return FilePath.from(self.head:sub(start).."/"..self.file)
end

---@param serverAddr string
---@param serverDir string
---@param localDir string?
local function load(serverAddr, serverDir, localDir)
    localDir = localDir or shell.dir()
    ---@type table<string, string>
    local handle, err = http.get("http://"..serverAddr.."/"..serverDir)
    if not handle then print(err and "[ERR-1] "..err or "couldn't connect to server") return end
    if handle.getResponseCode() ~= 200 then
        print("cannot download: "..handle.readAll())
        return
    end
    local data = handle.readAll() handle.close()
    data = data:sub(1, #data - 1)
    data = data:split(";")
    for _, path in pairs(data) do
        local path = FilePath.from(path)
		path = path:removeRoot()
        if #path.head > 0 then
            print("creating directory: "..path.head)
            fs.makeDir(path.head) -- create the directory
        end
        if #path.file > 0 then
            print("downloading: "..path:full())
            local handle, err = http.get("http://"..serverAddr.."/"..path:full(), {
                file = "yes"
            })
            if not handle then print(err and "[ERR-2] "..err or "couldn't connect to server") return end
            if handle.getResponseCode() ~= 200 then
                print("cannot download: "..handle.readAll())
                return
            end
            local data = handle.readAll() handle.close()
            print("creating file: "..path.file)
            local file = io.open(path:full(), "w") -- create the file
            if file then
                file:write(data)
                file:close()
			else
				print("file couldn't be created")
            end
        end
    end
end

---@param serverAddr string
---@param dirName string
local function create(serverAddr, dirName)
    ---@type table<string, string>
    local handle, err = http.create(serverAddr.."/"..dirName)
end

local args = {...}
local serverAddr = args[1]
if serverAddr then
    local command = args[2]
    if command then
        if command == "load" then
            local directory, localPath = args[3], args[4]
            if directory then
                load(serverAddr, directory, localPath)
            else
                print "no directory given"
            end
        else
            print("unknown command: "..command)
        end
    else
        print "USAGE:"
        print "  fts <address> load <directory> [<local path>]"
        print "  fts <address> create <directory> [<local path>]"
        print "  fts <address> upload <directory> <local path> <server path>"
    end
end

-- fts 127.0.0.1:7878 load subfolder