#![allow(unused_must_use, dead_code)]
use std::collections::HashMap;
use std::path::Path;
use std::{env, fs};
use std::net::{TcpListener, TcpStream};
use std::io::prelude::*;

mod http;
mod files;
use files::File;
use http::*;

#[derive(Debug)]
struct Server {
    dir: String,
}
impl Server {
    pub fn new(dir: String) -> Self {
        Self { dir }
    }
    pub fn handle_connection(&mut self, mut stream: TcpStream) {
        let Some(request) = HTTPRequest::from_stream(&mut stream) else { return };
        println!("request {:?}: {request:#?}", stream.peer_addr().ok());
        let response: HTTPResponse = match request {
            HTTPRequest { typ: HTTPRequestType::GET, path, headers, body: None } => {
                let full_path = self.dir.clone() + &path;
                if headers.contains_key("file") {
                    if let Ok(content) = fs::read_to_string(&full_path) {
                        let content = content.chars().filter_map(|c| if c != '\r' { Some(c) } else { None }).collect();
                        HTTPResponse { typ: HTTPResponseType::OK, headers: HashMap::new(), body: Some(content) }
                    } else {
                        HTTPResponse { typ: HTTPResponseType::NotFound, headers: HashMap::new(), body: Some(full_path) }
                    }
                } else {
                    if let Some(file_structure) = File::from_path(&Path::new(&full_path)) {
                        HTTPResponse { typ: HTTPResponseType::OK, headers: HashMap::new(), body: Some(file_structure.to_string()) }
                    } else {
                       HTTPResponse { typ: HTTPResponseType::NotFound, headers: HashMap::new(), body: Some(full_path) }
                    }
                }
            }
            _ => return
        };
        dbg!(&response);
        stream.write(response.to_string().as_bytes());
    }
}


fn main() {
    let mut args = env::args().skip(1);
    let Some(dir) = args.next() else { eprintln!("no directory given"); return };
    let mut server = Server::new(dir);
    let listener = TcpListener::bind("127.0.0.1:7878").unwrap();
    for stream in listener.incoming() {
        let Ok(stream) = stream else {
            continue;
        };
        server.handle_connection(stream);
    }
}