use std::path::Path;

#[derive(Debug, Clone)]
pub enum File {
    Path(String),
    Directory(String, Vec<Self>)
}
impl File {
    pub fn from_path(path: &Path) -> Option<Self> {
        if path.exists() {
            if path.is_dir() {
                let read_dir = path.read_dir().unwrap();
                let mut files = vec![];
                for entry in read_dir {
                    let Ok(entry) = entry else { continue; };
                    let Some(file) = Self::from_path(entry.path().as_path()) else { continue; };
                    files.push(file);
                }
                Some(Self::Directory(path.display().to_string(), files))
            } else if path.is_file() {
                Some(Self::Path(path.display().to_string()))
            } else {
                None
            }
        } else {
            None
        }
    }
}
impl ToString for File {
    fn to_string(&self) -> String {
        match self {
            File::Path(path) => path.clone(),
            File::Directory(_, files) => {
                let mut files = files.iter().map(|file| file.to_string()).collect::<Vec<String>>();
                files.sort();
                files.sort_by_key(|s| s.len());
                files.join(";")
            }
        }
    }
}