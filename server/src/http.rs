use std::{collections::HashMap, net::TcpStream, io::Read};

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum HTTPRequestType {
    GET, POST
}
#[derive(Debug, Clone, PartialEq)]
pub struct HTTPRequest {
    pub typ: HTTPRequestType,
    pub path: String,
    pub headers: HashMap<String, String>,
    pub body: Option<String>
}
impl HTTPRequest {
    pub fn from_stream(stream: &mut TcpStream) -> Option<Self> {
        let mut read = [0; 1024];
    
        stream.read(&mut read);
        let read = String::from_utf8_lossy(&read);
        let read = read.trim_matches(char::from(0));
    
        let mut parts = read.split("\r\n").collect::<Vec<&str>>();
        let head = if parts.get(0).is_some() { parts.remove(0) } else { return None };
    
        if head.starts_with("GET") {
            let Some((_, head)) = head.split_once(" ") else { return None };
            let Some((path, protocol)) = head.split_once(" ") else { return None };
            if protocol != "HTTP/1.1" { return None }
            let mut request = HTTPRequest {
                typ: HTTPRequestType::GET,
                path: path.to_string(),
                headers: HashMap::new(),
                body: None
            };
            for part in parts {
                let part = part.to_string();
                let mut chars = part.chars();
                let mut key = String::new();
                let mut value = String::new();
                while let Some(c) = chars.next() {
                    if c == ':' {
                        chars.next(); // skip whitespace
                        break;
                    }
                    key.push(c);
                }
                while let Some(c) = chars.next() {
                    value.push(c);
                }
                if key.len() > 0 {
                    if value.len() == 0 {
                        request.body = Some(key);
                    } else {
                        request.headers.insert(key, value);
                    }
                }
            }
            Some(request)
        } else if head.starts_with("POST") {
            todo!();
        } else {
            None
        }
    }
}
impl ToString for HTTPRequest {
    fn to_string(&self) -> String {
        let mut string = String::new();
        string.push_str(format!("{:?}", self.typ).as_str());
        string.push(' ');
        string.push_str(self.path.as_str());
        string.push_str(" HTTP/1.1\r\n");
        for (key, value) in &self.headers {
            string.push_str(key);
            string.push_str(": ");
            string.push_str(value);
            string.push_str("\r\n");
        }
        string.push_str("\r\n");
        if let Some(body) = &self.body {
            string.push_str(body);
        }
        string.push_str("\r\n");
        string
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum HTTPResponseType {
    Continue = 100,
    SwitchingProtocols = 101,
    Processing = 102,
    EarlyHints = 103,
    OK = 200,
    Created = 201,
    Accepted = 202,
    NonAuthoritativeInformation = 203,
    NoContent = 204,
    ResetContent = 205,
    PartialContent = 206,
    MultiStatus = 207,
    AlreadyReported = 208,
    IMUsed = 226,
    MultipleChoices = 300,
    MovedPermanently = 301,
    Found = 302,
    SeeOther = 303,
    NotModified = 304,
    TemporaryRedirect = 307,
    PermanentRedirect = 308,
    BadRequest = 400,
    Unauthorized = 401,
    PaymentRequired = 402,
    Forbidden = 403,
    NotFound = 404,
    MethodNotAllowed = 405,
    NotAcceptable = 406,
    ProxyAuthenticationRequired = 407,
    RequestTimeout = 408,
    Conflict = 409,
    Gone = 410,
    LengthRequired = 411,
    PreconditionFailed = 412,
    ContentTooLarge = 413,
    URITooLong = 414,
    UnsupportedMediaType = 415,
    RangeNotSatisfiable = 416,
    ExpectationFailed = 417,
    Teapot = 418,
    MisdirectedRequest = 421,
    UnprocessableContent = 422,
    Locked = 423,
    FailedDependency = 424,
    TooEarly = 425,
    UpgradeRequired = 426,
    PreconditionRequired = 428,
    TooManyRequests = 429,
    RequestHeaderFieldsTooLarge = 431,
    UnavailableForLegalReasons = 451,
    InternalServerError = 500,
    NotImplemented = 501,
    BadGateway = 502,
    ServiceUnavailable = 503,
    GatewayTimeout = 504,
    HTTPVersionNotSupported = 505,
    VariantAlsoNegotiates = 506,
    InsufficientStorage = 507,
    LoopDetected = 508,
    NotExtended = 510,
    NetworkAuthenticationRequired = 511
}
impl HTTPResponseType {
    fn code(&self) -> u16 {
        match self {
            HTTPResponseType::Continue => 100,
            HTTPResponseType::SwitchingProtocols => 101,
            HTTPResponseType::Processing => 102,
            HTTPResponseType::EarlyHints => 103,
            HTTPResponseType::OK => 200,
            HTTPResponseType::Created => 201,
            HTTPResponseType::Accepted => 202,
            HTTPResponseType::NonAuthoritativeInformation => 203,
            HTTPResponseType::NoContent => 204,
            HTTPResponseType::ResetContent => 205,
            HTTPResponseType::PartialContent => 206,
            HTTPResponseType::MultiStatus => 207,
            HTTPResponseType::AlreadyReported => 208,
            HTTPResponseType::IMUsed => 226,
            HTTPResponseType::MultipleChoices => 300,
            HTTPResponseType::MovedPermanently => 301,
            HTTPResponseType::Found => 302,
            HTTPResponseType::SeeOther => 303,
            HTTPResponseType::NotModified => 304,
            HTTPResponseType::TemporaryRedirect => 307,
            HTTPResponseType::PermanentRedirect => 308,
            HTTPResponseType::BadRequest => 400,
            HTTPResponseType::Unauthorized => 401,
            HTTPResponseType::PaymentRequired => 402,
            HTTPResponseType::Forbidden => 403,
            HTTPResponseType::NotFound => 404,
            HTTPResponseType::MethodNotAllowed => 405,
            HTTPResponseType::NotAcceptable => 406,
            HTTPResponseType::ProxyAuthenticationRequired => 407,
            HTTPResponseType::RequestTimeout => 408,
            HTTPResponseType::Conflict => 409,
            HTTPResponseType::Gone => 410,
            HTTPResponseType::LengthRequired => 411,
            HTTPResponseType::PreconditionFailed => 412,
            HTTPResponseType::ContentTooLarge => 413,
            HTTPResponseType::URITooLong => 414,
            HTTPResponseType::UnsupportedMediaType => 415,
            HTTPResponseType::RangeNotSatisfiable => 416,
            HTTPResponseType::ExpectationFailed => 417,
            HTTPResponseType::Teapot => 418,
            HTTPResponseType::MisdirectedRequest => 421,
            HTTPResponseType::UnprocessableContent => 422,
            HTTPResponseType::Locked => 423,
            HTTPResponseType::FailedDependency => 424,
            HTTPResponseType::TooEarly => 425,
            HTTPResponseType::UpgradeRequired => 426,
            HTTPResponseType::PreconditionRequired => 428,
            HTTPResponseType::TooManyRequests => 429,
            HTTPResponseType::RequestHeaderFieldsTooLarge => 431,
            HTTPResponseType::UnavailableForLegalReasons => 451,
            HTTPResponseType::InternalServerError => 500,
            HTTPResponseType::NotImplemented => 501,
            HTTPResponseType::BadGateway => 502,
            HTTPResponseType::ServiceUnavailable => 503,
            HTTPResponseType::GatewayTimeout => 504,
            HTTPResponseType::HTTPVersionNotSupported => 505,
            HTTPResponseType::VariantAlsoNegotiates => 506,
            HTTPResponseType::InsufficientStorage => 507,
            HTTPResponseType::LoopDetected => 508,
            HTTPResponseType::NotExtended => 510,
            HTTPResponseType::NetworkAuthenticationRequired => 511
        }
    }
}
#[derive(Debug, Clone, PartialEq)]
pub struct HTTPResponse {
    pub typ: HTTPResponseType,
    pub headers: HashMap<String, String>,
    pub body: Option<String>
}
impl ToString for HTTPResponse {
    fn to_string(&self) -> String {
        let mut string = String::new();
        string.push_str(format!("HTTP/1.1 {} {:?}\r\n", self.typ.code(), self.typ).as_str());
        for (key, value) in &self.headers {
            string.push_str(key);
            string.push_str(": ");
            string.push_str(value);
            string.push_str("\r\n");
        }
        string.push_str("\r\n");
        if let Some(body) = &self.body {
            string.push_str(body);
        }
        string.push_str("\r\n");
        string
    }
}